<?php
class uniba_de_api_lib_1 {
        function data_tables_1($params) {
                #$params ['source'] = str_replace(' ', '+', $params ['source']);

                $rows = json_decode ( file_get_contents ($params ['source']) );

                $rows = self::sort_by_whitelist($rows, $params);
                                $rows = self::merge_fields($rows, $params);     

                #todo
                #if (is_array($params ['key_whitelist'])){
                #       $rows = self::key_whitelist ( $rows, $params ['key_whitelist'] );
                #}
                // echo "CC";
                // print_r($params['source']);
                // print_r(sizeof($rows));


                $source = $params ['source'];
                if(!$params['hide_source_top']){
                  $string = 'Datenquelle <a href="'.$source.'">'.$source.'</a>';
                }


                foreach ( $rows as $row ) {
                        $i ++;
                        if ($i == 1) {
                                $string .= self::data_tables_1_header ( $params, $row );
                        }
                        $tr = '<tr>';
                        foreach($params['highlight_rows'] as $highlight_row){
                          if($row[$highlight_row['key']]){
                            $tr = $highlight_row['tr'];
                          }
                        }

                        $string .= $tr;

                        foreach ( $row as $key => $val ) {
                                if(self::key_in_whitelist($key, $params)){
                                        $string .= self::show_td($params, $row, $key, $val);
                                }
                        }
                        if($_REQUEST['info_all'] == 1){
                                $string .= '<td><ul>';

                                $sorter = function($pattern, $row){
                                        foreach ( $row as $key => $val) {
                                                if(preg_match("%$pattern%", $key)) {
                                                        $string .= "<li>$key:</li>";
                                                        $string .= "<ul><li>$val</li></ul>";
                                                }
                                        }
                                        return $string;
                                };

                                $string .= $sorter('^atom_', $row);
                                $string .= $sorter('^zki_', $row);
                                $string .= $sorter('(^)(?!(atom_))', $row);

                                $string .= '</ul></td>';
                        }

                                $string .= '</tr>';
                        }



                $string .= '</table>';
                if($params['source_footer_content']){
                  $string .= $params['source_footer_content'];
                }else{
                  $string .= '<p id="api.rz.uni-bamberg.de-datenquelle"> Datenquelle <a href="'.$params ['source'].'">'.$params ['source'].'</a></p>';
                }
                return $string;
        }
        function data_tables_1_header($params, $row) {
                $string = '<table id="1" border="1"><thead><tr>';
                foreach ( $row as $key => $val ) {
                        if(self::key_in_whitelist($key, $params)){
                                if($params['key_label'][$key]){
                                  $label = $params['key_label'][$key];
                                }else{
                                  $label = $key;
                                }
                                $string .= '<th>' . $label . '</th>';
                        }
                }
                if($_REQUEST['info_all'] == 1){
                        $string .=      '<th>info_all</th>';
                }
                $string .= '</tr></thead>';
                
 
                $string .= '<tfoot><tr>';
                foreach ( $row as $key => $val ) {
                        if(self::key_in_whitelist($key, $params)){
                                if($params['key_label'][$key]){
                                  $label = $params['key_label'][$key];
                                }else{
                                  $label = $key;
                                }
                                $string .= '<th>' . $label . '</th>';
                        }
                }
                if($_REQUEST['info_all'] == 1){
                        $string .=      '<th>info_all</th>';
                }
                $string .= '</tr></tfoot>';
                
                
                
                
                return $string;
        }

        function td($params, $key, $val){
                        $td = '<td ';
                        foreach($params['highlight_tds'] as $highlight_td){
                                if($key == $highlight_td['key']){
                                        if(preg_match("%".$highlight_td['pattern']."%", $val)){
                                        #if($val == $highlight_td['val'] ){
                                                $td .= $highlight_td['td'];
                                        }
                                }
                        }

                $td .= ' >';
                return $td;
        }

        function show_td($params, $row, $key, $val){
                $string = self::td($params, $key, $val);
                #$string = '<td>';
                $linked = '0';
                if(in_array($key, $params ['links'])){
                        foreach ( $params ['links'] as $link_key) {
                                $label = '';
                                $target = '';
                                $link = '';
                                $prefix = '';
                                if ($key == $link_key) {
                                        #####################
                                        if($params ['link'][$link_key]['label']){
                                                if($val){
                                                        $label = $params ['link'][$link_key]['label'];
                                                }
                                        }else{
                                                $label = $val;
                                        }
                                        #####################
                                        if($params ['link'][$link_key]['target']){
                                                #if($val){
                                                $target = $params ['link'][$link_key]['target'];
                                                #}
                                        }else{
                                                $target = $key;
                                        }
                                        #####################
                                        $prefix = $params ['link'][$link_key]['prefix'];

                                        #####################
                                        if($params ['link'][$link_key]['link']){
                                                if($row [$params ['link'][$link_key]['link']]!=''){
                                                        $link = $row [$params ['link'][$link_key]['link']];
                                                }
                                        }else{
                                                $link = $val;
                                        }
                                        #####################
                                        if($link==""){ #leerer Link verlinkt auch sich selber
                                                $linked = '0';
                                        }else{
                                                $string .= '<a target="' . $target . '" href="' . $prefix . $link . '">'.$label.'</a>';
                                                $linked = '1';
                                        }

                                }

                        }
                }elseif($params['filter_links'][$key]){
                        unset ($arr);
                        $arr [toggle_key] = $key;
                        $arr [toggle_key_prefix] = 'filter_';
                        $arr [toggle_val] = $val;
                        $string .= self::link_toggle_param ( $val, $arr );
                }else{
                          #$string .= self::local_nl2br($key, $val, $params);
                          $val = self::more_1($key, $val, $params);
                          $val = self::local_nl2br($key, $val, $params);
                          $val = self::local_markdown($key, $val, $params); 
                          $string .= $val;                             

                }
                $string .= '</td>';
                return $string;
        }


        function merge_fields($rows, $params) {
                if($params['merge_fields'] == false){
                        return $rows;
                }
                foreach ( $rows as $row ) {
                        unset($row_new);
                        foreach ( $row as $key => $val ) {
                                $row_new[$key] = $val;
                        }                                       

                        
                        #foreach ( $row_new as $key => $val ) {
                                
                                
                                foreach ($params['merge_fields'] as $merge_fields){
                                          #print_r($row);
                                          #print_r($row[$merge_fields['from']]);
                                          #print_r($merge_fields['to']);
                                        if($row_new[$merge_fields['from']] != ""){
                                                                                  $row_new[$merge_fields['to']] = $row_new[$merge_fields['to']].$merge_fields['infix'].$row_new[$merge_fields['from']].$merge_fields['suffix'];                           
                                        }                                          
                                        unset($row_new[$merge_fields['from']]);
                                }                               
                                
                                
                                
                        #}                                      
                        $rows_new [] = $row_new;                                
                }
                return $rows_new;
        }               
        
        
        function key_whitelist($rows, $key_whitelist) {
                foreach ( $rows as $row ) {
                        foreach ( $key_whitelist as $white ) {
                                foreach ( $row as $key => $val ) {

                                        // if (in_array ( $key, $key_whitelist )) {
                                        if ($key == $white) {
                                                $row_new [$key] = $val;
                                        }
                                }
                        }
                        $rows_new [] = $row_new;
                }
                // print_r($rows_new);
                return $rows_new;
        }
        function sort_by_whitelist($rows, $params) {
                if($params['key_whitelist'] == false){
                        return $rows;
                }
                foreach ( $rows as $row ) {
                        unset($row_new);
                        #einfügen analog aus reihenfolge whitelist
                        foreach ($params['key_whitelist'] as $white){
                                $row_new[$white] = $row->$white;
                        }
                        #keys noch einfügen die nicht in der whitelist enthalten sind
                        foreach ($row as $key => $val){
                                if(!isset($row_new[$key])){
                                        $row_new[$key] = $val;
                                }
                        }
                        $rows_new [] = $row_new;
                }
                return $rows_new;
        }

        function key_in_whitelist($key, $params) {
                if($params['key_whitelist'] == false){
                        return true;
                }
                if (in_array($key, $params['key_whitelist'])) {
                        return true;
                }else{
                        return false;
                }
        }


//                 <link rel="stylesheet" type="text/css"
//                         href="https://api.rz.uni-bamberg.de/html/lib/css/1.css">
// 
//                 <link rel="stylesheet" type="text/css"
//                         href="/html/lib/datatables.net/1.10.3/css/jquery.dataTables.css">        
        

        function data_tables_1_script_header($params="") {

                if($params['sorting']){
                        $sorting = $params['sorting'];
                }else{
                        $sorting = '"aaSorting": [[ 1, "asc" ]],';

                }        
                
                if($params['search']){
                        $search = $params['search'];
                }else{
                        $search = '';
                }
                
                
                
                if($params['css']['1.css'] == True){
                  $string .= '
                  <link rel="stylesheet" type="text/css"
                          href="/json-to-datatables.php/ext/1.css">                                    
                  ';
                  }                
                if($params['css']['jquery.dataTables.min.css'] == True){
                  $string .= '
                <link rel="stylesheet" type="text/css"
                        href="/json-to-datatables.php/ext/jquery.dataTables.min.css">  
                  ';
                  }  

                if($params['css']['dataTables_filter-default-1'] != False){
                  $string .= ' 
                  <style>
                        .dataTables_filter {
                          /*background: #EDEDED; !important;*/
                          padding: 12px 12px 12px 12px !important;
                          margin-bottom: 15px;
                          border: solid;
                          border-width: 0px;
                          border-color: #B2B6B2;          
                      }
                  </style>                  
                  ';
                  }else{
                  $string .= ' 
                  <style>
                        .dataTables_filter {
                          visibility: hidden;    
                          display: none;
                      }
                  </style>                  
                  ';                  
                  
                  }                   
                if($params['css']['dataTables-uniba-default-1'] == True){
                  $string .= '   
                  <style>                        
                      .dataTables_paginate {
                          visibility:hidden;
                      }
                      .dataTables_info {
                          visibility:hidden;
                      }
                      .dataTables_length {
                          visibility:hidden;
                      }
                      tfoot input {
                              width: 100%;
                              padding: 3px;
                              box-sizing: border-box;
                          }  
                      tfoot {
                          display: table-header-group;
                      }                         
                  </style>                    
                  ';
                  }   
                if($params['css']['XXtfoot-1']){
                  $string .= '     
 
                  ';
                  }   
                if($params['css']['XXtfoot-1']){
                  $string .= '                  
                  ';
                  }                     

                  
                $string .= <<< EOT


                

               
                
                <script type="text/javascript" charset="utf8"
                        src="/json-to-datatables.php/ext/jquery-1.11.3.min.js"></script>
                <script type="text/javascript" charset="utf8"
                        src="/json-to-datatables.php/ext/jquery.dataTables.min.js"></script>
                <script type="text/javascript" language="javascript" src="/json-to-datatables.php/ext/moment.min.js">
                </script>
                <script type="text/javascript" language="javascript" src="/json-to-datatables.php/ext/datetime-moment.js"></script>                
                        
                        <script type="text/javascript">
                          $(document).ready(function() {
                             //https://datatables.net/blog/2014-12-18
                             $.fn.dataTable.moment ( 'DD.MM.YYYY HH:mm' );
                              // Setup - add a text input to each footer cell
                              $('#1 tfoot th').each( function () {
                                  var title = $(this).text();
                                  $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                              } );
                          
                              // DataTable
                              var table = $('#1').DataTable( {                              
                                  "search": {
                                    "search": "$search"
                                  },                              
                                    "bJQueryUI": true,
                                    "bStateSave": false,
                                     "iDisplayLength": 500,
                                     $sorting
                                  } );
                          
                              // Apply the search
                              table.columns().every( function () {
                                  var that = this;
                          
                                  $( 'input', this.footer() ).on( 'keyup change', function () {
                                      if ( that.search() !== this.value ) {
                                          that
                                              .search( this.value )
                                              .draw();
                                      }
                                  } );
                              } );
                          } );
                        </script>








EOT;
                return $string;
        }
        function data_tables_1_html_js_1($params) {

                if($params['sorting']){
                        $sorting = $params['sorting'];
                }else{
                        $sorting = '"aaSorting": [[ 1, "desc" ]],';

                }


                $string = <<< EOT



                <script type="text/javascript" charset="utf-8">
//                         $('#1').ready(function() {
//                                 $.fn.dataTable.moment( 'D.M.YYYY H:mm' );
//                     
//                                 var oTable = $('#1').dataTable( {
//                                     "bJQueryUI": false,
//                                     "bStateSave": false,
//                                      "iDisplayLength": 500,
//                                                                         $sorting
//                                   } );
//                         } );
                </script>

EOT;


                return $string;
        }
        function filter_link($param) {
                if ($param == '') {
                        return '';
                }
                $string .= '<div class="filter_link"><span id="filter" value="' . $param . '">[-]</span>';
                $string .= '<span id="filter" value="' . $param . '">[+]</span></div> ';

                return $string;
        }

        function local_markdown($key, $val, $params) {
                        if (in_array ( $key, $params['markdown'] )){
                          include_once ("/usr/share/php/markdown.php");
                          $val = Markdown($val);
                         }
        return $val;        
        }
        function local_nl2br($key, $val, $params) {
                        if (in_array ( $key, $params['nl2br'] )){
                          #include_once ("/usr/share/php/markdown.php");
                          $val = nl2br($val);
                         }
        return $val;        
        }        
        function more_1($key, $val, $params) {
                        if (in_array ( $key, $params['more_1'] )){
                        global $accordion_count;
                        $accordion_count ++;
                        $accordion_prefix = '  
                        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
                        <script>
                        $(function() {
                          $( "#accordion'.$GLOBALS[accordion_count].'" ).accordion();
                        });
                        </script>                        
                        
                        <div id="accordion'.$GLOBALS[accordion_count].'">
                          <h3>Section 1</h3>
                          <div>
                            <p>                        
                        ';     
                        $accordion_suffix = '                
                            </p>
                          </div>                   
                        ';                          
                        unset($matches);
                        $pattern = "%(.{500})(.+)%xs"; #s (PCRE_DOTALL) 
                        if(preg_match($pattern, $val, $matches)){
                          $val = $matches[1]."... [mehr] ";
                        }else{                       
                          $val = $val;
                        }
                         ;
                         if($_REQUEST[debug] == 1){
                            print_r($matches);  
                            #print_r($matches[0][2]);                         
                         }

                        #echo "<br>".$val;
                        
                        }
        return $val;        
        }        
        
        
        
        function link_toggle_param($title, $params){
                $key = $params['toggle_key_prefix'].$params['toggle_key'];
                #$key = $params['toggle_key'];
                $query_data[$key.'[]'] = $params[toggle_val];
                $query_string = self::build_url($query_data, $params);
                $string = '<a href="'.$query_string.'">'.$title.'</a>';


                parse_str($_SERVER['QUERY_STRING'], $request);

                #print_r($request);
                #echo "XX ";
                #print_r($key);
                #echo " ";

                if($request[$key][0] == $params[toggle_val]){
                        $query_data[$key][0] = '';
                        $query_data[$key.'[]'] = '';
                        #unset($query_data[$key.'[]']);
                        #unset($query_data[$key.'[0]']);
                        $params['hide_arrays'] = true;
                        #print_r($query_data);
                        $query_string = self::build_url($query_data, $params);
                        $string = $title.' <a  href="'.$query_string.'">[-]</a>';
                }


                return $string;
        }
        function build_url($query_data_new,$params){
                parse_str($_SERVER['QUERY_STRING'], $query_data);
                $query_data = array_merge($query_data, $query_data_new);
                foreach($query_data as $key => $val){
                        if (is_array($val)){
                                if ($val[0] != ''){
                                        $query_data_mod[$key] = $val;
                                }
                        }elseif ($val != ''){
                                $query_data_mod[$key] = $val;
                        }
                }
                $query_data = $query_data_mod;
                if($path){
                        $string = $path;
                }else{
                        $string = $_SERVER[PHP_SELF];
                }
                $string .= '?';
                $string .= http_build_query($query_data);
                #echo "XXX".$string;
                #[0] -> []
                $string = str_replace('%5B0%5D','%5B%5D',$string);
                return $string;
        }
}
