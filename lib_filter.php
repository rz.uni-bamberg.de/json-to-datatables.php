<?php
#https://api.rz.uni-bamberg.de/api/ticketsystem/v1/current/public/messages/search/service_id/2
class uniba_de_api_lib_filter
{
    function filter($params)
    {
        $params ['source'] = str_replace(' ', '+', $params ['source']);
        $rows = json_decode ( file_get_contents ($params ['source']) );
        #print_r($_REQUEST);
        unset ($filters);
//         if(
//             ($_SERVER['QUERY_STRING']=='')
//             OR
//             ($_SERVER['QUERY_STRING']=='debug=1')           
//             ){
//             return (json_encode($rows));
//         }


        parse_str($_SERVER['QUERY_STRING'], $request_vars);
        foreach ($request_vars as $request_key => $request_val ) {
            $filter_prefix='filter_';
            if(preg_match("%^$filter_prefix%", $request_key)){
               $request_key = preg_replace("%^$filter_prefix%", '',$request_key);
               $filters[$request_key]  = $request_val;
            }
        }
        if(!$filters){
            return (json_encode($rows));
        }

        $rows_neu = new stdclass();
        $i=0;
        foreach ($rows as $row) {
            $i++;
            #print_r($_REQUEST);
            #
            $y=0;
            foreach ( $filters as $key_filters => $val_filters) {
                $y++;
                if(!is_array($key_filters)){#todo geht nicht ?
                    #echo "drin";
                    #echo $key_filters;
                    $temp_key_filters = $key_filters;
                    $temp_val_filters = $val_filters;
                    #print_r($val_filters);
                    unset($key_filters);
                    unset($val_filters);
                    $key_filters[] = $temp_key_filters;
                    $val_filters[0][0][] = $temp_val_filters;
                }
                foreach ($key_filters as $key) {
                    $matching_for_keys[$y] = False;
                    #print_r($key);
                    foreach($val_filters[0][0][0] as $val){
                        #print_r($val);

                        if (sizeof($rows_neu->$i)>0){
                            $row_subject = $rows_neu->$i;
                        }else{
                            $row_subject = $row;
                        }
                        if (self::filter_row($row_subject, $key, $val)){
                            $matching_for_keys[$y] = True;
                        }
                    }
###
                }
            }

            #print_r($matching_for_keys);
            foreach ($matching_for_keys as $matching_for_key) {
                $rows_neu->$i = $row;
                if($matching_for_key == False){
                    unset($rows_neu->$i);
                    break;
                }
            }

            #$rows_neu->$i->$row_neu;
        }

        $string = '[';
        $i=0;
        foreach ( $rows_neu as $row_neu) {
            $i++;
            if ($i>1){
                $string .= ', ';
            }
            $string .=  json_encode($row_neu);
        }
        $string .=  ']';
        return ($string);
    }
    function filter_row($row, $key, $select){
        if(preg_match("%^(now_)(.+)%", $key, $matches)){
            #echo "<br> ".$row->$matches[2];
            #Sat, 26 Sep 2015 22:07:32 GMT
            #echo "<br>".$row->$matches[2];
            $val_unix = strtotime($row->$matches[2]);
            if(preg_match("%d%",$select)){
                $select = str_replace('d', '', $select);
                $select = $select * 1440;
            }
            if(preg_match("%m%",$select)){
                $select = str_replace('m', '', $select);
                $select = $select * 60;
            }
            if($select < 0){
                $select = $select*(-1);
                #echo "<br>".$val_unix;
                #echo "<br>-".(time()-2000);
                $select_limit = (time()-($select*60));
                #echo "<br>".time();
                #echo "<br>-".$select_limit;
                if($val_unix > $select_limit){
                    if($val_unix < time()) {
                        return ($row);
                    }
                }
            }elseif
            ($select > 0){

                #echo "<br>".$val_unix;
                #echo "<br>-".(time()-2000);
                $select_limit = (time()+($select*60));
                #echo "<br>".time();
                #echo "<br>-".$select_limit;
                if($val_unix < $select_limit){
                    if($val_unix > time()){
                        return($row);
                    }

                }
            }


            #echo "<br>-".strtotime($row->$matches[2]);
            #echo "<br>--".date('%c', $unix);
            #echo "<br>".date('%a, %d %b %Y %H:%M:%S %Z', $row->$matches[2]);
            #echo "<br>".(date('a, d b Y H:M:S Z', strtotime($row->$matches[2])));
            #$now = new DateTime("now");
            #print_r($now);
            #echo " ".$row->$key;
            #echo "$select ";
        }
        elseif($row->$key == $select){
            #echo " id:";
            # print_r($row->id);
            return($row);
        }else{
            #unset($row);
            return(False);
        }
    }
}


?>